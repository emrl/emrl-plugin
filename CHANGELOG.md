# Changelog

## [Unreleased][unreleased]

## [3.0.5] - 2024-07-25
### Changed
- Hide chat bubble and show link in admin bar instead

## [3.0.4] - 2023-08-17
### Added
- Added fix for new core CSS transient in WordPress 6.3

## [3.0.3] - 2023-02-08
### Added
- Add action and command for resetting theme roots

### Fixed
- Added `.gitattributes` file to ignore tests and config on export

## [3.0.2] - 2021-04-30
### Fixed
- Fixed typo in `Admin\LiveChat` script

## [3.0.1] - 2021-04-30
### Fixed
- Use correct action name for `Admin\LiveChat` header scripts

## [3.0.0] - 2021-04-30
Complete rewrite  
[Changelog for previous versions](https://bitbucket.org/emrl/emrl-plugin/src/2.0.9/CHANGELOG.md)

[unreleased]: https://bitbucket.org/emrl/emrl-plugin/branches/compare/master%0D3.0.5
[3.0.5]: https://bitbucket.org/emrl/emrl-plugin/branches/compare/3.0.5%0D3.0.4
[3.0.4]: https://bitbucket.org/emrl/emrl-plugin/branches/compare/3.0.4%0D3.0.3
[3.0.3]: https://bitbucket.org/emrl/emrl-plugin/branches/compare/3.0.3%0D3.0.2
[3.0.2]: https://bitbucket.org/emrl/emrl-plugin/branches/compare/3.0.2%0D3.0.1
[3.0.1]: https://bitbucket.org/emrl/emrl-plugin/branches/compare/3.0.1%0D3.0.0
[3.0.0]: https://bitbucket.org/emrl/emrl-plugin/branches/compare/3.0.0%0D2.0.9
