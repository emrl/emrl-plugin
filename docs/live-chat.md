# Live Chat

Enables live chat (via [Sunshine Conversations](https://smooch.io/)) on the WordPress admin (on production environment).

Example:

```php
(new Emrl\Admin\LiveChat())->register();
```
