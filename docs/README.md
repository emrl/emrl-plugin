# EMRL Plugin

A collection of useful utilities to help us quickly build plugins and themes.

## Installation

`composer require emrl/emrl ^3`

## Usage

Utilities are meant to be used inside your plugin or theme's bootstrap file
(ie: `functions.php`).

1. [Live Chat](live-chat.md)
