<?php

declare(strict_types=1);

namespace Emrl\Tests\Admin;

use Emrl\Admin\LiveChat;
use Emrl\Tests\TestCase;

use function Brain\Monkey\Functions\when;

final class LiveChatTest extends TestCase
{
    public function testActionsAdded(): void
    {
        $instance = (new LiveChat())->register();
        $this->assertIsInt(has_action('admin_print_scripts', [$instance, 'header']));
        $this->assertIsInt(has_action('admin_print_footer_scripts', [$instance, 'footer']));
    }

    public function testHeader(): void
    {
        $this->expectOutputRegex('/"'.preg_quote(LiveChat::INTEGRATION_ID).'"/');
        (new LiveChat())->header();
    }

    public function testFooter(): void
    {
        $email = 'test@test.com';

        when('wp_get_current_user')->justReturn((object) [
            'user_email' => $email,
            'user_login' => 'test',
            'display_name' => 'Test',
        ]);

        $this->expectOutputRegex('/"'.preg_quote($email).'"/');
        (new LiveChat())->footer();
    }
}
