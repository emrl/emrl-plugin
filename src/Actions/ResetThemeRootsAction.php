<?php

declare(strict_types=1);

namespace Emrl\Actions;

class ResetThemeRootsAction
{
    /**
     * Resets the cached theme roots
     *
     * This is necessary when using symlink deployments and installing themes
     * outside of the default directory.
     */
    public function __invoke(): void
    {
        if (!is_blog_installed()) {
            return;
        }

        // Reset options
        delete_option('template_root');
        delete_option('stylesheet_root');

        // `delete_transient()` does not seem to work here
        delete_option('_site_transient_theme_roots');

        // Delete WordPress 6.3 transient for core CSS (absolute paths are saved)
        delete_transient('wp_core_block_css_files');

        // Cache new theme directory paths
        search_theme_directories(true);

        // Set the new roots
        update_option('template_root', get_raw_theme_root(wp_get_theme(get_stylesheet())->get_template(), true));
        update_option('stylesheet_root', get_raw_theme_root(get_stylesheet(), true));

        // Rebuild rewrites
        flush_rewrite_rules();
    }
}
