<?php

declare(strict_types=1);

namespace Emrl\Admin;

use WP_Admin_Bar;

class LiveChat
{
    public const INTEGRATION_ID = '59a843f9db9c1128002cef71';

    public function register(): self
    {
        add_action('admin_bar_menu', [$this, 'adminBarMenu'], 999);
        add_action('admin_print_scripts', [$this, 'header']);
        add_action('admin_print_footer_scripts', [$this, 'footer']);

        return $this;
    }

    public function adminBarMenu(WP_Admin_Bar $bar): void
    {
        if (is_admin()) {
            $bar->add_menu([
                'id' => 'emrl-support',
                'title' => '<span class="ab-icon dashicons dashicons-format-chat"></span> Support Chat',
                'href' => '#',
                'parent' => 'top-secondary',
            ]);
        }
    }

    public function header(): void
    {
        // phpcs:disable Generic.Files.LineLength.TooLong
        printf('
            <script>!function(o,p,s,e,c){var i,a,h,u=[],d=[];function t(){var t="You must provide a supported major version.";try{if(!c)throw new Error(t);var e,n="https://cdn.smooch.io/",r="smooch";if((e="string"==typeof this.response?JSON.parse(this.response):this.response).url){var o=p.getElementsByTagName("script")[0],s=p.createElement("script");s.async=!0;var i=c.match(/([0-9]+)\.?([0-9]+)?\.?([0-9]+)?/),a=i&&i[1];if(i&&i[3])s.src=n+r+"."+c+".min.js";else{if(!(4<=a&&e["v"+a]))throw new Error(t);s.src=e["v"+a]}o.parentNode.insertBefore(s,o)}}catch(e){e.message===t&&console.error(e)}}o[s]={init:function(){i=arguments;var t={then:function(e){return d.push({type:"t",next:e}),t},catch:function(e){return d.push({type:"c",next:e}),t}};return t},on:function(){u.push(arguments)},render:function(){a=arguments},destroy:function(){h=arguments}},o.__onWebMessengerHostReady__=function(e){if(delete o.__onWebMessengerHostReady__,o[s]=e,i)for(var t=e.init.apply(e,i),n=0;n<d.length;n++){var r=d[n];t="t"===r.type?t.then(r.next):t.catch(r.next)}a&&e.render.apply(e,a),h&&e.destroy.apply(e,h);for(n=0;n<u.length;n++)e.on.apply(e,u[n])};var n=new XMLHttpRequest;n.addEventListener("load",t),n.open("GET","https://"+e+".webloader.smooch.io/",!0),n.responseType="json",n.send()}(window,document,"Smooch","%1$s","5");</script>
        ', static::INTEGRATION_ID);
        // phpcs:enable
    }

    public function footer(): void
    {
        $user = wp_get_current_user();

        printf(
            '
                <style>#web-messenger-container:not(.open){block-size:0;inline-size:0;pointer-events:none;}</style>
                <script>
                    Smooch.init({
                        integrationId: "%1$s",
                        businessName: "EMRL Support",
                        buttonWidth: "48px",
                        buttonHeight: "48px",
                    }).then(function() {
                        Smooch.updateUser({
                            email: "%2$s",
                            givenName: "%3$s",
                            metadata: {
                                username: "%4$s",
                            },
                        })

                        const chat = document.getElementById("web-messenger-container")

                        document.querySelector("#wp-admin-bar-emrl-support .ab-item").addEventListener("click", (e) => {
                            e.preventDefault()

                            if (chat.classList.contains("open")) {
                                Smooch.close()
                                chat.classList.remove("open")
                            } else {
                                chat.classList.add("open")
                                Smooch.open()
                            }
                        })
                    })
                </script>
            ',
            static::INTEGRATION_ID,
            $user->user_email,
            $user->display_name,
            $user->user_login,
        );
    }
}
