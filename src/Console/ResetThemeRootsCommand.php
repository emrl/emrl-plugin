<?php

declare(strict_types=1);

namespace Emrl\Console;

use Emrl\Actions\ResetThemeRootsAction;
use WP_CLI;

class ResetThemeRootsCommand
{
    public function register(): self
    {
        WP_CLI::add_command('theme roots reset', $this, [
            'shortdesc' => 'Resets the cached theme roots',
            // phpcs:ignore Generic.Files.LineLength.TooLong
            'longdesc' => 'This is necessary when using symlink deployments and installing themes outside of the default directory.',
        ]);

        return $this;
    }

    public function __invoke(): void
    {
        (new ResetThemeRootsAction())();
        WP_CLI::success('Theme roots reset.');
    }
}
